#include <iostream>
#include <string>
#include <random>

int main(int argc, char** argv) {
    auto N = (argc == 1) ? 25U : std::stoi(argv[1]);
    auto r = std::random_device{}; // /dev/random
    auto d = std::normal_distribution<double>{1000, 500};
    auto chop = [](double x){ return static_cast<long>(x); };
    for (auto k = 0U; k < N; ++k) {
        std::cout << chop(d(r)) << " " << chop(d(r)) << " " << chop(d(r)) << "\n";
    }
}
