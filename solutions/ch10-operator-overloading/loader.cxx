#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include "vector-3d.hxx"

using std::cout;
using std::cin;
namespace rm = ribomation::types;

int main() {
    auto vectors = std::vector<rm::Vector3d<long>>{};
    for (rm::Vector3d<long> v; cin >> v; ) vectors.push_back(v);
    cout << "loaded " << vectors.size() << " vectors from stdin\n";

    auto sorted = std::set<rm::Vector3d<long>>{vectors.cbegin(), vectors.cend()};
    for (auto&& v : sorted) cout << v << "\n";
}
