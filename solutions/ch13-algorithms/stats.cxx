#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
using std::cin;
using std::cout;
using std::begin;
using std::end;

int main() {
    auto values = std::vector<double>{};
    for (double x; cin >> x;) values.push_back(x);
    cout << "loaded          : " << values.size() << " values\n";

    auto [smallest, largest] = std::minmax_element(begin(values), end(values));
    cout << "smallest/largest: " << *smallest << "/" << *largest << "\n";

    auto mean = std::accumulate(begin(values), end(values), 0.0) / values.size();
    cout << "mean value      : " << mean << "\n";

    auto diff = [mean](double x) { return x - mean; };
    auto sq = [](double x) { return x * x; };
    auto stddev = std::accumulate(begin(values), end(values), 0.0, [&diff, &sq](double sum, double x) {
        return sum + sq(diff(x));
    });
    stddev = ::sqrt(stddev/values.size());
    cout << "std. deviation  : " << stddev << "\n";
}
