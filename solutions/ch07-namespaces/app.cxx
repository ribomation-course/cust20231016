#include <iostream>
#include "lib.hxx"

int main() {
    using std::cout;
    namespace rm = ribomation::lib;
    cout << rm::func1() << "\n";
    cout << rm::func2() << "\n";
    cout << rm::func3() << "\n";
}
