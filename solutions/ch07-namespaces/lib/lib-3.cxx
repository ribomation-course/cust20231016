#include <string>

namespace {
    std::string const message = "G'day from func3()";
}

namespace ribomation::lib {
    auto func3() { return message; }
}
