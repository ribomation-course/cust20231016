#include <string>

namespace {
    std::string const message = "Tjabba from func2()";
}

namespace ribomation::lib {
    auto func2() { return message; }
}
