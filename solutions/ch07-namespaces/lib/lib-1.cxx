#include <string>

namespace {
    std::string const message = "Hello from func1()";
}

namespace ribomation::lib {
    auto func1() { return message; }
}
