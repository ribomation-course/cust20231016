#include <iostream>
#include <string>

using std::cout;
using std::string;
using namespace std::literals;

template<typename T>
bool equals(T const& a, T const& b, T const& c) {
    return (a == b) && (b == c);
}

template<>
bool equals(double const& a, double const& b, double const& c) {
    auto eps = .00001;
    return (std::abs(a - b) < eps) && (std::abs(b - c) < eps);
}

int main() {
    cout << std::boolalpha;
    cout << "int   : " << equals(42, 20 + 20 + 2, 2 * 3 * 7) << "\n";
    cout << "string: " << equals("Wi-Fi"s, "Wi"s + "-"s + "Fi"s, "Wiki"s.substr(0, 2) + "-"s + "Finland"s.substr(0, 2))
         << "\n";
    cout << "char  : " << equals('X', (char) 88, char('A' + 23)) << "\n";
    cout << "double: " << equals(1.5, 1.25 + .25, -1.0 + 2.5);
}
