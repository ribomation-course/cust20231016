#include <iostream>
#include <utility>

using std::cout;

template<typename ReadIter, typename WriteIter, typename UnaryFunc>
void apply(ReadIter first, ReadIter last, WriteIter dest, UnaryFunc fn) {
    for (; first != last; ++first, ++dest) *dest = fn(*first);
}

int main() {
    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    auto const N = std::size(numbers);

    for (auto x: numbers) cout << x << " ";
    cout << "\n";
    apply(numbers, numbers + N, numbers, [](int x) { return x * x; });
    for (auto x: numbers) cout << x << " ";
    cout << "\n----\n";

    int arr[N]{};
    for (auto x: arr) cout << x << " ";
    cout << "\n";
    apply(std::cbegin(numbers), std::cend(numbers), arr, [](int x) { return x; });
    for (auto x: arr) cout << x << " ";
    cout << "\n";
}
