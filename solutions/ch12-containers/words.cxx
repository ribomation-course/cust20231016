#include <iostream>
#include <string>
#include <unordered_set>
#include <set>

int main() {
    auto words1 = std::unordered_set<std::string>{};
    for (std::string w; std::cin >> w;) words1.emplace(w);
    std::cout << "loaded " << words1.size() << " words\n";

    auto words2 = std::set<std::string>{words1.cbegin(), words1.cend()};
    for (auto&& w: words2) std::cout << w << " ";
    std::cout << "\n";
}
