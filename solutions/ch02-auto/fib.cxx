#include <iostream>
#include <map>

using std::cout;

auto fib(auto n) {
    if (n <= 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

struct Data {
    int arg{};
    unsigned res{};
};

auto compute(auto n) {
    return Data{n, fib(n)};
}

auto populate(auto n) {
    auto tbl = std::map<int, unsigned>{};
    for (auto k = 1; k <= n; ++k) {
        auto [arg, res] = compute(k);
        tbl[arg] = res;
    }
    return tbl;
}

int main() {
    auto const N = 10;
    {
        cout << "-- 1 ----\n";
        auto r = fib(N);
        cout << "fib(" << N << ") = " << r << "\n";
    }
    {
        cout << "-- 2 ----\n";
        auto [arg, res] = compute(N);
        cout << "fib(" << arg << ") = " << res << "\n";
    }
    {
        cout << "-- 3 ----\n";
        for (auto [arg, res]: populate(N))
            cout << "fib(" << arg << ") = " << res << "\n";
    }
}
