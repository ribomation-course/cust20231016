#include <iostream>
#include <algorithm>
#include <functional>
using std::cout;

auto count_if(int* first, const int* last, std::function<bool(int)> const& pred) {
    auto cnt = 0U;
    for (; first != last; ++first) if (pred(*first)) ++cnt;
    return cnt;
}

int main() {
    {
        cout << "-- 1 ----\n";
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);

        auto print = [](auto x) { cout << x << " "; };
        std::for_each(numbers, numbers + N, print);
        cout << "\n";

        auto factor = 42;
        std::transform(numbers, numbers + N, numbers, [factor](auto x) { return x * factor; });

        std::for_each(numbers, numbers + N, print);
        cout << "\n";
    }
    cout << "------\n";
    {
        cout << "-- 2 ----\n";
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        auto const N = sizeof(numbers) / sizeof(numbers[0]);

        auto c = count_if(numbers, numbers + N, [](auto x) { return x % 3 == 0; });
        cout << "c = " << c << "\n";
    }
}
