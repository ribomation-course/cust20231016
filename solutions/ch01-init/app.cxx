#include <iostream>
#include <string>
#include <vector>
using std::cout;
using std::string;
using namespace std::string_literals;
int main() {
    {
        std::vector<string> words = {
                "C++"s, "is"s, "a"s, "cool"s, "language"s
        };
        for (string& s : words) cout << s << " ";
        cout << "\n";
    }
    cout << "----\n";
    {
        string sentence = "It was a dark and silent night."s;
        if (unsigned long idx = sentence.find("and"s); idx != string::npos) {
            cout << sentence.substr(idx) << "\n";
        }

    }
}
