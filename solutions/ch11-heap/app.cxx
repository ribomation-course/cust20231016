#include <iostream>
#include <vector>
#include <chrono>
#include <cstring>

using std::cout;
namespace cr = std::chrono;

char* copystr(char const* str) { //::strdup
    if (str == nullptr) return nullptr;
    return ::strcpy(new char[::strlen(str) + 1], str);
}

class Person {
    char* name = nullptr;
    unsigned age = 0;
    inline static int count = 0;
    inline static bool verbose = true;
public:
    Person() {
        ++count;
        if (verbose)
            cout << "Person{} @ " << this << " (" << count << ")\n";
    }

    Person(char const* name_, unsigned age_)
            : name{copystr(name_)}, age{age_} {
        ++count;
        if (verbose)
            cout << "Person{" << name << ", " << age << "} @ " << this << " &name="
                 << (void*) name << " (" << count
                 << ")\n";
    }

    Person(Person const& rhs)
            : name{copystr(rhs.name)}, age{rhs.age} {
        ++count;
        if (verbose)
            cout << "Person{Person const&} @ " << this << " &name=" << (void*) name << " (" << count << ")\n";
    }

    Person(Person&& rhs) noexcept
            : name{rhs.name}, age{rhs.age} {
        rhs.name = nullptr;
        rhs.age = 0;
        ++count;
        if (verbose)
            cout << "Person{&&} @ " << this << " &name=" << (void*) name << " (" << count << ")\n";
    }

    ~Person() {
        --count;
        if (verbose)
            cout << "~Person{" << (name ? name : "??") << "} @ " << this << " (" << count << ")\n";
        delete[] name;
    }

    friend auto operator<<(std::ostream& os, Person const& rhs) -> std::ostream& {
        return os << "Person{" << (rhs.name ? rhs.name : "??") << ", " << rhs.age << "}";
    }

    static auto getCount() { return count; }

    static void setVerbose(bool b) { verbose = b; }
};

int main() {
//    auto const N = 10U;
    auto const N = 1'000'000U;
    if (N > 25U) Person::setVerbose(false);

    auto start = cr::high_resolution_clock::now();
    {
        cout << "[main] -- 1 ---\n";
        cout << "[main] creating " << N << " objects...\n";
        auto v = std::vector<Person*>{};        // <--- (1)
        v.reserve(N);
        for (auto k = 0U; k < N; ++k) {
            auto p = new Person{"Nisse", k};    // <--- (2)
            v.push_back(p);
        }
        cout << "[main::inner] count=" << Person::getCount() << "\n";

        cout << "[main] disposing " << N << " objects...\n";
        for (auto ptr : v) delete ptr;          // <--- (3)
    }
    cout << "[main] count=" << Person::getCount() << "\n";

    auto end = cr::high_resolution_clock::now();
    auto elapsed = cr::duration_cast<cr::microseconds>(end-start).count();
    cout << "[main] elapsed=" << elapsed << " us\n";

    auto ptr = new Person{"Inge Vidare", 45};
    cout<<*ptr;
    delete ptr;
}
