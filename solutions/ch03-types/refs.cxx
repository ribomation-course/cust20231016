#include <iostream>

using std::cout;

void fn(int a, int& b, int&& c) {
    a *= 10;
    b *= 20;
    c *= 30;
}

int main() {
    int number = 42;
    cout << "(a) number=" << number << "\n";
    fn(number, number, number * 5);
    cout << "(b) number=" << number << "\n";
}
