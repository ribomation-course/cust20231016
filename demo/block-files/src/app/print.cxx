#include <iostream>
#include <string>
#include <locale>
#include "block-file.hxx"
#include "account.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;
using namespace ribomation::bank;


int main() {
    auto const file = "account.db"s;
    auto      total = 0;
    auto      db    = BlockFile<Account>{file};
    for (auto acc: db) {
        cout << acc << endl;
        total += acc;  // acc.operator int()
    }
    cout << "total balance: SEK " << total << endl;
}

